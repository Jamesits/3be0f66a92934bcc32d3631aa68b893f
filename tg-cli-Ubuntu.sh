#!/bin/bash
# for Ubuntu

sudo apt-get install build-essential libreadline-dev libconfig-dev libssl-dev lua5.2 liblua5.2-dev libevent-dev libjansson-dev libpython-dev make 
git clone https://github.com/vysheng/tg.git --recursive
cd tg
./configure
make