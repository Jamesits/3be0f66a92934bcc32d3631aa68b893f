#!/bin/bash
# for macOS

brew install libconfig readline lua python libevent jansson openssl
ln -s /usr/local/opt/readline/lib/libreadline.7.dylib /usr/local/opt/readline/lib/libreadline.6.dylib
git clone https://github.com/vysheng/tg.git --recursive
cd tg
CFLAGS="-I/usr/local/include -I/usr/local/Cellar/readline/6.3.8/include" \
  	LDFLAGS="-L/usr/local/lib -L/usr/local/Cellar/readline/6.3.8/lib" \
  	./configure --with-openssl=/usr/local/opt/openssl
sed -i -e 's/-Werror//g' Makefile
make